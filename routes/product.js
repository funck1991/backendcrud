var express = require('express');
const { request } = require('../app');
var router = express.Router();
var Product = require('../model/product');

/* GET users listing. */
//listar
router.get('/', function(req, res, next) {
  Product.find({}, function(err, resp) {
    if (err) res.send(err);
    else res.send(resp);
    //console.log(resp);
  });
});


//agregar
router.post('/', function(request, res, next) {
  var product = new Product({

    firstName: request.body.firstName,
    company: request.body.company,
    lastName: request.body.lastName,
    address: request.body.address,
    city: request.body.city,
    state: request.body.state,
    postalCode: request.body.postalCode,
    shipping: request.body.shipping
  });
  product.save(function(err, resp) {
    if (err) 
      res.send(err);
    else {
      res.send(resp);
    }
  });
});



//inicio metodo modificar

router.put('/', function(req, res, next){
console.log(req.body);
var _id = req.body.id;

if (_id != undefined) {
  Product.updateOne(
    {
      _id: _id
    },
    {
      $set: {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        address: req.body.address,
        city: req.body.city,
        state: req.body.state,
        postalCode: req.body.postalCode,
        shipping: req.body.shipping
      }
    },
    //inicio funcion error
    function(err, resp) {
      if (err) res.sendStatus(err);
      else {
        res.send(resp);

      }
    });//fin funcion error 
  }});
//fin metodo modificar



//inicio metodo delete
router.delete('/', function(req, res, next){

  var _id = req.body._id;
  
  if (_id != undefined) {//si la id no es indefinida
    Product.deleteOne({_id: _id},//schema.deleteOne
      //inicio funcion error
      function(err, resp) {
        if (err) res.sendStatus(err);
        else {
          res.send(resp);
  
        }
      }//fin funcion error 
    );
  }});

//fin metodo delete
module.exports = router;
