var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var companySchema = new Schema({

  firstName: String,
  company: String,
  lastName: String,
  address: String,
  city: String,
  mail: String,
  state: String,
  postalCode: String,
  shipping: String

});
module.exports = mongoose.model('Company', companySchema, 'company');
