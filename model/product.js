var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({

  firstName: String,
  company: String,
  lastName: String,
  address: String,
  city: String,
  mail: String,
  state: String,
  postalCode: String,
  shipping: String

});
module.exports = mongoose.model('Product', productSchema, 'product');
